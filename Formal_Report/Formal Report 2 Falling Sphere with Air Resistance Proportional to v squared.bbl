% $ biblatex auxiliary file $
% $ biblatex bbl format version 3.2 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated as
% required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup

\datalist[entry]{nty/global//global/global}
  \entry{numpy}{article}{}
    \name{author}{26}{}{%
      {{hash=HCR}{%
         family={Harris},
         familyi={H\bibinitperiod},
         given={Charles\bibnamedelima R.},
         giveni={C\bibinitperiod\bibinitdelim R\bibinitperiod},
      }}%
      {{hash=MKJ}{%
         family={Millman},
         familyi={M\bibinitperiod},
         given={K.\bibnamedelima Jarrod},
         giveni={K\bibinitperiod\bibinitdelim J\bibinitperiod},
      }}%
      {{hash=vdWSJ}{%
         prefix={van\bibnamedelima der},
         prefixi={v\bibinitperiod\bibinitdelim d\bibinitperiod},
         family={Walt},
         familyi={W\bibinitperiod},
         given={St{\'{e}}fan\bibnamedelima J.},
         giveni={S\bibinitperiod\bibinitdelim J\bibinitperiod},
      }}%
      {{hash=GR}{%
         family={Gommers},
         familyi={G\bibinitperiod},
         given={Ralf},
         giveni={R\bibinitperiod},
      }}%
      {{hash=VP}{%
         family={Virtanen},
         familyi={V\bibinitperiod},
         given={Pauli},
         giveni={P\bibinitperiod},
      }}%
      {{hash=CD}{%
         family={Cournapeau},
         familyi={C\bibinitperiod},
         given={David},
         giveni={D\bibinitperiod},
      }}%
      {{hash=WE}{%
         family={Wieser},
         familyi={W\bibinitperiod},
         given={Eric},
         giveni={E\bibinitperiod},
      }}%
      {{hash=TJ}{%
         family={Taylor},
         familyi={T\bibinitperiod},
         given={Julian},
         giveni={J\bibinitperiod},
      }}%
      {{hash=BS}{%
         family={Berg},
         familyi={B\bibinitperiod},
         given={Sebastian},
         giveni={S\bibinitperiod},
      }}%
      {{hash=SNJ}{%
         family={Smith},
         familyi={S\bibinitperiod},
         given={Nathaniel\bibnamedelima J.},
         giveni={N\bibinitperiod\bibinitdelim J\bibinitperiod},
      }}%
      {{hash=KR}{%
         family={Kern},
         familyi={K\bibinitperiod},
         given={Robert},
         giveni={R\bibinitperiod},
      }}%
      {{hash=PM}{%
         family={Picus},
         familyi={P\bibinitperiod},
         given={Matti},
         giveni={M\bibinitperiod},
      }}%
      {{hash=HS}{%
         family={Hoyer},
         familyi={H\bibinitperiod},
         given={Stephan},
         giveni={S\bibinitperiod},
      }}%
      {{hash=vKMH}{%
         prefix={van},
         prefixi={v\bibinitperiod},
         family={Kerkwijk},
         familyi={K\bibinitperiod},
         given={Marten\bibnamedelima H.},
         giveni={M\bibinitperiod\bibinitdelim H\bibinitperiod},
      }}%
      {{hash=BM}{%
         family={Brett},
         familyi={B\bibinitperiod},
         given={Matthew},
         giveni={M\bibinitperiod},
      }}%
      {{hash=HA}{%
         family={Haldane},
         familyi={H\bibinitperiod},
         given={Allan},
         giveni={A\bibinitperiod},
      }}%
      {{hash=dRJF}{%
         prefix={del},
         prefixi={d\bibinitperiod},
         family={R{\'{i}}o},
         familyi={R\bibinitperiod},
         given={Jaime\bibnamedelima Fern{\'{a}}ndez},
         giveni={J\bibinitperiod\bibinitdelim F\bibinitperiod},
      }}%
      {{hash=WM}{%
         family={Wiebe},
         familyi={W\bibinitperiod},
         given={Mark},
         giveni={M\bibinitperiod},
      }}%
      {{hash=PP}{%
         family={Peterson},
         familyi={P\bibinitperiod},
         given={Pearu},
         giveni={P\bibinitperiod},
      }}%
      {{hash=GMP}{%
         family={G{\'{e}}rard-Marchant},
         familyi={G\bibinithyphendelim M\bibinitperiod},
         given={Pierre},
         giveni={P\bibinitperiod},
      }}%
      {{hash=SK}{%
         family={Sheppard},
         familyi={S\bibinitperiod},
         given={Kevin},
         giveni={K\bibinitperiod},
      }}%
      {{hash=RT}{%
         family={Reddy},
         familyi={R\bibinitperiod},
         given={Tyler},
         giveni={T\bibinitperiod},
      }}%
      {{hash=WW}{%
         family={Weckesser},
         familyi={W\bibinitperiod},
         given={Warren},
         giveni={W\bibinitperiod},
      }}%
      {{hash=AH}{%
         family={Abbasi},
         familyi={A\bibinitperiod},
         given={Hameer},
         giveni={H\bibinitperiod},
      }}%
      {{hash=GC}{%
         family={Gohlke},
         familyi={G\bibinitperiod},
         given={Christoph},
         giveni={C\bibinitperiod},
      }}%
      {{hash=OTE}{%
         family={Oliphant},
         familyi={O\bibinitperiod},
         given={Travis\bibnamedelima E.},
         giveni={T\bibinitperiod\bibinitdelim E\bibinitperiod},
      }}%
    }
    \list{publisher}{2}{%
      {Springer Science}%
      {Business Media {LLC}}%
    }
    \strng{namehash}{HCR+1}
  \strng{fullhash}{HCRMKJWSJvdGRVPCDWETJBSSNJKRPMHSKMHvBMHARJFdWMPPGMPSKRTWWAHGCOTE1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{H}
    \field{sortinithash}{H}
    \verb{doi}
    \verb 10.1038/s41586-020-2649-2
    \endverb
    \field{number}{7825}
    \field{pages}{357\bibrangedash 362}
    \field{title}{Array programming with {NumPy}}
    \verb{url}
    \verb https://doi.org/10.1038/s41586-020-2649-2
    \endverb
    \field{volume}{585}
    \field{journaltitle}{Nature}
    \field{month}{09}
    \field{year}{2020}
  \endentry

  \entry{matplotlib}{online}{}
    \name{author}{1}{}{%
      {{hash=HJ}{%
         family={Hunter},
         familyi={H\bibinitperiod},
         given={John},
         giveni={J\bibinitperiod},
      }}%
    }
    \strng{namehash}{HJ1}
    \strng{fullhash}{HJ1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{H}
    \field{sortinithash}{H}
    \field{title}{Matplotlib}
    \verb{url}
    \verb https://matplotlib.org/
    \endverb
    \field{year}{2003}
    \field{urlday}{30}
    \field{urlmonth}{04}
    \field{urlyear}{2024}
  \endentry

  \entry{IEEE-754}{book}{}
    \name{author}{1}{}{%
      {{hash=I}{%
         family={{IEEE-754}},
         familyi={I\bibinitperiod},
      }}%
    }
    \list{publisher}{1}{%
      {IEEE}%
    }
    \strng{namehash}{I1}
    \strng{fullhash}{I1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{I}
    \field{sortinithash}{I}
    \field{abstract}{%
    This standard specifies interchange and arithmetic formats and methods for
  binary and decimal floating-point arithmetic in computer programming
  environments. This standard specifies exception conditions and their default
  handling. An implementation of a floating-point system conforming to this
  standard may be realized entirely in software, entirely in hardware, or in
  any combination of software and hardware. For operations specified in the
  normative part of this standard, numerical results and exceptions are
  uniquely determined by the values of the input data, sequence of operations,
  and destination formats, all under user control.%
    }
    \verb{doi}
    \verb https://doi.org/10.1109/IEEESTD.2019.876622
    \endverb
    \field{isbn}{1-5044-5925-3 (print), 1-5044-5924-5 (e-PDF)}
    \field{pages}{82}
    \field{title}{{IEEE 754-2019, Standard for Floating-Point Arithmetic}}
    \list{location}{1}{%
      {New York, NY, USA}%
    }
    \field{year}{2019}
  \endentry

  \entry{raylib}{online}{}
    \name{author}{1}{}{%
      {{hash=r}{%
         family={raysan},
         familyi={r\bibinitperiod},
      }}%
    }
    \strng{namehash}{r1}
    \strng{fullhash}{r1}
    \field{labelnamesource}{author}
    \field{labeltitlesource}{title}
    \field{sortinit}{R}
    \field{sortinithash}{R}
    \field{title}{Raylib}
    \verb{url}
    \verb https://www.raylib.com/
    \endverb
    \field{year}{2013}
    \field{urlday}{30}
    \field{urlmonth}{04}
    \field{urlyear}{2024}
  \endentry
\enddatalist
\endinput
