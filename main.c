#include "raylib.h"
#include "stdio.h"
#include "stdlib.h"

const int SCREEN_HEIGHT = 800;
const int SCREEN_WIDTH = 1000;

typedef struct vector_t {
  double x;
  double y;
} Vector;

typedef struct object_t {
  Vector position;
  Vector velocity;
  Vector acceleration;
  float drag;
  float area;
  float mass;
} Object;

typedef struct graph_t {
  int x1;          // the left most part of the graph
  int y1;          // the top of the graph
  int x2;          // the right most part of the graph
  int y2;          // the bottom of the graph
  double y_min;    // the minimum y value that is on the graph
  double y_max;    // the maximum y value that is on the graph
  double x_min;    // the minimum x value on the graph
  double x_max;    // the maximum x value on the graph
  double y_scale;  // used to fit all the data on the graph
  double x_scale;  // used to fit all the data on the graph
  double y_offset; // Where the x-axis is placed
  double x_offset; // where the y-axis is placed
} Graph;

void resizeDouble(double **list, int *size, int length) {
  if (length >= *size) {
    *size = 2 * (*size);
    *list = (double *)realloc((void *)(*list), (*size) * sizeof(double));
  }
}

void resizeVector(Vector **list, int *size, int length) {
  if (length >= *size) {
    *size = 2 * (*size);
    *list = (Vector *)realloc((void *)(*list), (*size) * sizeof(Vector));
  }
}

void SetGraphYMin(Graph *graph, float min) {
  if (min < graph->y_min) {
    graph->y_min = min;
  }
}

void SetGraphYMax(Graph *graph, float max) {
  if (max > graph->y_max) {
    graph->y_max = max;
  }
}
void SetGraphXMin(Graph *graph, float min) {
  if (min < graph->x_min) {
    graph->x_min = min;
  }
}

void SetGraphXMax(Graph *graph, float max) {
  if (max > graph->x_max) {
    graph->x_max = max;
  }
}

void SetGraphData(Graph *graph, double yMax, double yMin, double xMax,
                  double xMin) {
  SetGraphYMax(graph, yMax);
  SetGraphYMin(graph, yMin);
  SetGraphXMax(graph, xMax);
  SetGraphXMin(graph, xMin);
  graph->y_scale = (graph->y2 - graph->y1) / (graph->y_max - graph->y_min);
  graph->y_offset = graph->y1 + graph->y_max * graph->y_scale;
  graph->x_scale = (graph->x2 - graph->x1) / (graph->x_max - graph->x_min);
  graph->x_offset = graph->x_scale * graph->x_min + graph->x1;
}

void DrawGraphPoint(Graph graph, float x, float y, float radius, Color color) {
  DrawCircle(x * graph.x_scale + graph.x_offset,
             graph.y_offset - y * graph.y_scale, radius, color);
}

void DrawGraphAxes(Graph graph, Color color) {
  DrawLine(graph.x1, graph.y_offset, graph.x2,
           graph.y_scale * graph.y_max + graph.y1, color);
  DrawLine(graph.x1, graph.y1, graph.x1, graph.y2, color);
}

void DrawGraphLabels(Graph graph, const char *title, int fontSize,
                     Color color) {
  DrawText(title, (graph.x2 - graph.x1) / 2.0 + graph.x1, graph.y1, fontSize,
           color);
  DrawText(TextFormat("%.3lf", graph.y_max), graph.x1 - 50, graph.y1, fontSize,
           color);
  DrawText(TextFormat("%.3lf", graph.y_min), graph.x1 - 50, graph.y2, fontSize,
           color);
  DrawText(TextFormat("%.3lf", graph.x_min), graph.x1 - 50, graph.y_offset,
           fontSize, color);
  DrawText(TextFormat("%.8lf", graph.x_max), graph.x2 + 10, graph.y_offset,
           fontSize, color);
}

int Simulate(Object ball, double **time, Vector **position, Vector **velocity,
             Vector **acceleration, double **potential_energy,
             double **kinetic_energy, double **energy) {
  const double delta_time = 0.00001;  // s
  const float gravity = 9.81f;        // N/kg or m/s^2
  const float medium_density = 1.293; // kg/m^3

  double current_time = 0.0;
  int time_size = 100;
  int time_length = 0;
  int position_size = 100;
  int velocity_size = 100;
  int acceleration_size = 100;
  int position_length = 0;
  int velocity_length = 0;
  int acceleration_length = 0;

  int potential_size = 100;
  int kinetic_size = 100;
  int energy_size = 100;
  int potential_length = 0;
  int kinetic_length = 0;
  int energy_length = 0;

  ball.position = (Vector){0.0f, 440.0f};

  ball.acceleration.y = -gravity + ball.drag * medium_density * ball.area *
                                       ball.velocity.y * ball.velocity.y /
                                       (2 * ball.mass);

  while (ball.position.y > 0.0f) {
    (*acceleration)[time_length] = ball.acceleration;
    (*velocity)[time_length] = ball.velocity;
    (*position)[time_length] = ball.position;
    (*time)[time_length] = current_time;
    (*potential_energy)[time_length] = ball.mass * gravity * ball.position.y;
    (*kinetic_energy)[time_length] =
        ball.mass * ball.velocity.y * ball.velocity.y / 2.0f;
    (*energy)[time_length] =
        (*potential_energy)[time_length] + (*kinetic_energy)[time_length];

    time_length += 1;
    resizeVector(acceleration, &acceleration_size, time_length);
    resizeVector(velocity, &velocity_size, time_length);
    resizeVector(position, &position_size, time_length);

    resizeDouble(time, &time_size, time_length);
    resizeDouble(potential_energy, &potential_size, time_length);
    resizeDouble(kinetic_energy, &kinetic_size, time_length);
    resizeDouble(energy, &energy_size, time_length);

    ball.acceleration.y = -gravity + ball.drag * medium_density * ball.area *
                                         ball.velocity.y * ball.velocity.y /
                                         (2 * ball.mass);
    ball.velocity.y += ball.acceleration.y * delta_time;
    ball.position.y += ball.velocity.y * delta_time;
    current_time += delta_time;
  }

  // Done to record the final transform of the ball.
  (*acceleration)[time_length] = ball.acceleration;
  (*velocity)[time_length] = ball.velocity;
  (*position)[time_length] = ball.position;
  (*time)[time_length] = current_time;
  (*potential_energy)[time_length] = ball.mass * gravity * ball.position.y;
  (*kinetic_energy)[time_length] =
      ball.mass / 2.0f * ball.velocity.y * ball.velocity.y;
  (*energy)[time_length] =
      (*potential_energy)[time_length] + (*kinetic_energy)[time_length];

  acceleration_length += 1;
  velocity_length += 1;
  position_length += 1;

  potential_length += 1;
  kinetic_length += 1;
  energy_length += 1;

  time_length += 1;

  resizeVector(acceleration, &acceleration_size, time_length);
  resizeVector(velocity, &velocity_size, time_length);
  resizeVector(position, &position_size, time_length);

  resizeDouble(time, &time_size, time_length);
  resizeDouble(potential_energy, &potential_size, time_length);
  resizeDouble(kinetic_energy, &kinetic_size, time_length);
  resizeDouble(energy, &energy_size, time_length);

  return time_length;
}

int main(void) {
  InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Falling Sphere with Air Resistance");
  SetTargetFPS(10);
  Object bowling_ball = (Object){(Vector){0.0f, 0.0f},
                                 (Vector){0.0f, 0.0f},
                                 (Vector){0.0f, 0.0f},
                                 0.5f,
                                 0.03742f,
                                 7.257f};

  Object base_ball = (Object){(Vector){0.0f, 0.0f},
                              (Vector){0.0f, 0.0f},
                              (Vector){0.0f, 0.0f},
                              0.5f,
                              0.017638f,
                              .149f};

  Object basket_ball = (Object){(Vector){0.0f, 0.0f},
                                (Vector){0.0f, 0.0f},
                                (Vector){0.0f, 0.0f},
                                0.5f,
                                1.76384f,
                                .625f};

  double *time;
  Vector *position;
  Vector *velocity;
  Vector *acceleration;
  double *potential_energy;
  double *kinetic_energy;
  double *energy;

  int size = 100;

  time = (double *)malloc(sizeof(double) * size);
  position = (Vector *)malloc(sizeof(Vector) * size);
  velocity = (Vector *)malloc(sizeof(Vector) * size);
  acceleration = (Vector *)malloc(sizeof(Vector) * size);
  potential_energy = (double *)malloc(sizeof(double) * size);
  kinetic_energy = (double *)malloc(sizeof(double) * size);
  energy = (double *)malloc(sizeof(double) * size);

  int time_length =
      Simulate(bowling_ball, &time, &position, &velocity, &acceleration,
               &potential_energy, &kinetic_energy, &energy);

  double current_time = time[time_length - 1];

  printf("%lf \n", time[time_length - 1]);
  printf("%lf \n", current_time);
  printf("%lf \n", position[time_length - 1].y);
  printf("%lf \n", velocity[time_length - 1].y);
  printf("%lf \n", acceleration[time_length - 1].y);

  // Position min and max are known values, but this is good practice, and these
  // values are needed later.
  double position_min = 1000000;
  double position_max = -1000000;
  double velocity_min = 1000000;
  double velocity_max = -1000000;
  double acceleration_min = 1000000;
  double acceleration_max = -1000000;

  double potential_min = 1000000;
  double kinetic_min = 1000000;
  double energy_min = 1000000;
  double potential_max = -1000000;
  double kinetic_max = -1000000;
  double energy_max = -1000000;

  for (int i = 0; i < time_length; i++) {
    if (potential_energy[i] < potential_min) {
      potential_min = potential_energy[i];
    }
    if (potential_energy[i] > potential_max) {
      potential_max = potential_energy[i];
    }

    if (kinetic_energy[i] < kinetic_min) {
      kinetic_min = kinetic_energy[i];
    }
    if (kinetic_energy[i] > kinetic_max) {
      kinetic_max = kinetic_energy[i];
    }

    if (energy[i] > energy_max) {
      energy_max = energy[i];
    }

    if (energy[i] < energy_min) {
      energy_min = energy[i];
    }

    if (acceleration[i].y < acceleration_min) {
      acceleration_min = acceleration[i].y;
    }
    if (acceleration[i].y > acceleration_max) {
      acceleration_max = acceleration[i].y;
    }

    if (velocity[i].y < velocity_min) {
      velocity_min = velocity[i].y;
    }

    if (velocity[i].y > velocity_max) {
      velocity_max = velocity[i].y;
    }

    if (position[i].y < position_min) {
      position_min = position[i].y;
    }

    if (position[i].y > position_max) {
      position_max = position[i].y;
    }
  }

  Graph position_graph = (Graph){SCREEN_WIDTH * 0.1f,
                                 SCREEN_HEIGHT * (1 / 24.0f),
                                 SCREEN_WIDTH * 0.4f,
                                 SCREEN_HEIGHT * (1.0f / 4.0f + 1 / 24.0f),
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0};

  Graph velocity_graph = (Graph){SCREEN_WIDTH * 0.1f,
                                 SCREEN_HEIGHT * (1.0f / 4.0f + 3.0f / 24.0f),
                                 SCREEN_WIDTH * 0.4f,
                                 SCREEN_HEIGHT * (2.0f / 4.0f + 3.0f / 24.0f),
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0,
                                 0};

  Graph acceleration_graph =
      (Graph){SCREEN_WIDTH * 0.1f,
              SCREEN_HEIGHT * (2.0f / 4.0f + 4.0f / 24.0f),
              SCREEN_WIDTH * 0.4f,
              SCREEN_HEIGHT * (3.0f / 4.0f + 4.0f / 24.0f),
              0,
              0,
              0,
              0,
              0,
              0,
              0,
              0};

  Graph potential_graph = (Graph){SCREEN_WIDTH * 0.6f,
                                  SCREEN_HEIGHT * (0.0f / 4.0f + 1.0f / 24.0f),
                                  SCREEN_WIDTH * 0.9f,
                                  SCREEN_HEIGHT * (1.0f / 4.0f + 1.0f / 24.0f),
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0,
                                  0};

  Graph kinetic_graph = (Graph){SCREEN_WIDTH * 0.6f,
                                SCREEN_HEIGHT * (1.0f / 4.0f + 3.0f / 24.0f),
                                SCREEN_WIDTH * 0.9f,
                                SCREEN_HEIGHT * (2.0f / 4.0f + 3.0f / 24.0f),
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0,
                                0};

  Graph energy_graph = (Graph){SCREEN_WIDTH * 0.6f,
                               SCREEN_HEIGHT * (2.0f / 4.0f + 4.0f / 24.0f),
                               SCREEN_WIDTH * 0.9f,
                               SCREEN_HEIGHT * (3.0f / 4.0f + 4.0f / 24.0f),
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0,
                               0};

  SetGraphData(&position_graph, position_max, position_min, current_time, 0.0f);
  SetGraphData(&velocity_graph, velocity_max, velocity_min, current_time, 0.0f);
  SetGraphData(&acceleration_graph, acceleration_max, acceleration_min,
               current_time, 0.0);
  SetGraphData(&potential_graph, potential_max, potential_min, current_time,
               0.0);
  SetGraphData(&kinetic_graph, kinetic_max, kinetic_min, current_time, 0.0);
  SetGraphData(&energy_graph, energy_max, energy_min, current_time, 0.0);

  int radius = 3;
  while (!WindowShouldClose()) {

    BeginDrawing();
    ClearBackground(BLACK);

    DrawGraphAxes(position_graph, RAYWHITE);
    DrawGraphAxes(velocity_graph, RAYWHITE);
    DrawGraphAxes(acceleration_graph, RAYWHITE);
    DrawGraphAxes(potential_graph, RAYWHITE);
    DrawGraphAxes(kinetic_graph, RAYWHITE);
    DrawGraphAxes(energy_graph, RAYWHITE);

    for (int i = 0; i < time_length; i++) {
      DrawGraphPoint(position_graph, time[i], position[i].y, radius, GREEN);
      DrawGraphPoint(velocity_graph, time[i], velocity[i].y, radius, BLUE);
      DrawGraphPoint(acceleration_graph, time[i], acceleration[i].y, radius,
                     RED);
      DrawGraphPoint(potential_graph, time[i], potential_energy[i], radius,
                     BLUE);
      DrawGraphPoint(kinetic_graph, time[i], kinetic_energy[i], radius, RED);
      DrawGraphPoint(energy_graph, time[i], energy[i], radius, GREEN);
    }

    DrawGraphLabels(position_graph, "position", 12, RAYWHITE);
    DrawGraphLabels(velocity_graph, "velocity", 12, RAYWHITE);
    DrawGraphLabels(acceleration_graph, "acceleration", 12, RAYWHITE);

    DrawGraphLabels(potential_graph, "potential energy", 12, RAYWHITE);
    DrawGraphLabels(kinetic_graph, "kinetic energy", 12, RAYWHITE);
    DrawGraphLabels(energy_graph, "total energy", 12, RAYWHITE);

    EndDrawing();
  }

  TakeScreenshot("bowling_ball_data_000001s.png");

  FILE *fptr;
  fptr = fopen("bowling_ball_data_000001s.csv", "w");
  fprintf(fptr,
          "time, position, velocity, acceleration, potential energy, kinetic "
          "energy, total energy\n");
  for (int i = 0; i < time_length; i++) {
    fprintf(fptr, "%lf, %lf, %lf, %lf, %lf, %lf, %lf\n", time[i], position[i].y,
            velocity[i].y, acceleration[i].y, potential_energy[i],
            kinetic_energy[i], energy[i]);
  }

  fclose(fptr);

  free(time);
  free(position);
  free(velocity);
  free(acceleration);
  free(potential_energy);
  free(kinetic_energy);
  free(energy);

  CloseWindow();
  return 0;
}
